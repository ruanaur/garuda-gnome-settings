#!/bin/sh
dconf load / < ~/.config/garuda-dconf.ini
rm -rf ~/.config/garuda-dconf.ini &
rm -rf ~/.config/autostart-scripts/dconf.sh &
 
notify-send "GNOME settings applied! 🔥"